# Oowlish Challenge

This is my quick take on the challenge. I'm aware it's not ideal (specially on the error handling part), but since I promised I'd ship this weekend, there it is.

# Setup

1.  Edit **.env-example** on both the **packages/client** and **packages/server** and rename it to **.env**
2.  Run **yarn install**
3.  Run **yarn start**

## Testing

1. Run **yarn test**

# Contact

Gustavo Caetano -> souljacker@gmail.com
