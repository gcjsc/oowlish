import React from 'react';
import ReactDOM from 'react-dom';
import { ChakraProvider } from '@chakra-ui/react';
import { Auth0Provider } from '@auth0/auth0-react';
import { BrowserRouter as Router } from 'react-router-dom';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';

import './index.css';
import App from './App';

const {
  REACT_APP_OAUTH_DOMAIN,
  REACT_APP_OAUTH_ID,
  REACT_APP_GRAPHQL_ENDPOINT,
} = process.env;

const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        customersByCity: {
          keyArgs: false,
          merge(existing, incoming) {
            return incoming;
          },
        },
      },
    },
  },
});

const client = new ApolloClient({
  uri: REACT_APP_GRAPHQL_ENDPOINT,
  cache,
});

ReactDOM.render(
  <Router>
    <ApolloProvider client={client}>
      <Auth0Provider
        domain={REACT_APP_OAUTH_DOMAIN}
        clientId={REACT_APP_OAUTH_ID}
        redirectUri={window.location.origin}
        cacheLocation='localstorage'
      >
        <ChakraProvider>
          <App />
        </ChakraProvider>
      </Auth0Provider>
    </ApolloProvider>
  </Router>,
  document.getElementById('root')
);
