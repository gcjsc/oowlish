import './App.css';
import { GridItem, Container } from '@chakra-ui/react';
import { Route } from 'react-router-dom';
import Login from './components/Login';
import DashboardMain from './components/DashboardMain';
import ProtectedRoute from './components/ProtectedRoute';
import Menu from './components/Menu';
import City from './components/City';
import Profile from './components/Profile';

function App() {
  return (
    <Container maxWidth='120ch' align='center'>
      <Menu />
      <GridItem alignSelf='center'>
        <Route path='/login' exact component={Login} />
        <ProtectedRoute path='/cities/:city' component={City} />
        <ProtectedRoute path='/profile/:id' component={Profile} />
        <ProtectedRoute path='/' exact component={DashboardMain} />
      </GridItem>
    </Container>
  );
}

export default App;
