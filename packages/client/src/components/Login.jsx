import React from 'react';
import { Box, Stack, Button, Container, Heading } from '@chakra-ui/react';
import { useAuth0 } from '@auth0/auth0-react';

export default function Login() {
  const { loginWithRedirect } = useAuth0();

  return (
    <Box width={{ base: '90%', md: '400px' }} bg='gray.50' rounded='lg' p={5}>
      <Heading marginBottom='1.5rem'>Sign in</Heading>
      <form>
        <Stack marginBottom='2rem'>
          <Container>
            You must be signed in in order to access the dashboard
          </Container>
        </Stack>
        <Stack marginBottom='2rem'>
          <Button colorScheme='blue' onClick={() => loginWithRedirect()}>
            Sign in
          </Button>
        </Stack>
      </form>
    </Box>
  );
}
