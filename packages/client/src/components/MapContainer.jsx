import React from 'react';
import { Map, GoogleApiWrapper } from 'google-maps-react';
const { REACT_APP_GOOGLE_GEO_API_KEY } = process.env;

const mapStyles = {
  position: 'relative',
  width: '50%',
  height: '80%',
};
const MapContainer = (props) => {
  return (
    <Map
      google={props.google}
      zoom={16}
      style={mapStyles}
      initialCenter={{
        lat: props.lat,
        lng: props.lng,
      }}
    />
  );
};

export default GoogleApiWrapper({
  apiKey: REACT_APP_GOOGLE_GEO_API_KEY,
})(MapContainer);
