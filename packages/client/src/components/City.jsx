import React from 'react';
import { useParams } from 'react-router-dom';
import { gql, useQuery } from '@apollo/client';
import { Grid, GridItem, Box, Button, Text, Heading } from '@chakra-ui/react';
import {
  EmailIcon,
  ChevronDownIcon,
  SunIcon,
  MoonIcon,
} from '@chakra-ui/icons';
import { useHistory } from 'react-router-dom';

export const CUSTOMERS_BY_CITY = gql`
  query CustomersByCity($city: String!, $cursor: Int) {
    customersByCity(city: $city, cursor: $cursor) {
      cursor
      hasMore
      hasLess
      customers {
        id
        first_name
        last_name
        email
        gender
        company
        title
      }
    }
  }
`;
export default function City() {
  const { city } = useParams();
  const history = useHistory();

  const { loading, data, fetchMore } = useQuery(CUSTOMERS_BY_CITY, {
    variables: { city },
    fetchPolicy: 'network-only',
  });

  if (loading) {
    return <div>Loading</div>;
  }

  if (!city) {
    return <div>City not found</div>;
  }

  const renderPagination = () => {
    if (data) {
      const { hasMore, hasLess, cursor } = data.customersByCity;

      return (
        <Grid
          templateColumns='minmax(20px, 100px) minmax(20px, 100px)'
          mt='20px'
          gap='20px'
          justifyContent='center'
        >
          <Button
            colorScheme='gray'
            disabled={!hasLess}
            onClick={() =>
              fetchMore({ variables: { cursor: parseInt(cursor) - 1 } })
            }
          >
            Back
          </Button>
          <Button
            colorScheme='gray'
            disabled={!hasMore}
            onClick={() =>
              fetchMore({ variables: { cursor: parseInt(cursor) + 1 } })
            }
          >
            More
          </Button>
        </Grid>
      );
    }
  };

  return (
    <>
      <Heading as='h2' size='2xl' mb='10'>
        {city}
      </Heading>
      <Grid templateColumns='repeat(auto-fit, minmax(300px, 1fr))' gap={6}>
        {data &&
          data.customersByCity.customers.map((item) => (
            <GridItem key={item.id}>
              <Box
                maxW='sm'
                borderWidth='1px'
                borderRadius='lg'
                overflow='hidden'
                padding='20px'
              >
                <Box
                  mt='2'
                  mb='10'
                  fontWeight='semibold'
                  as='h4'
                  lineHeight='tight'
                  isTruncated
                >
                  {item.gender === 'Male' ? (
                    <MoonIcon mr='2' />
                  ) : (
                    <SunIcon mr='2' />
                  )}{' '}
                  {item.first_name} {item.last_name}
                </Box>
                <Box mt='10' mb='2' align='left'>
                  <Grid templateColumns='20px 1fr' alignItems='center'>
                    <ChevronDownIcon w={3} h={3} />{' '}
                    <Text fontSize='sm'>
                      {item.title} at <strong>{item.company}</strong>
                    </Text>
                  </Grid>
                </Box>
                <Box mb='2' align='left'>
                  <Grid templateColumns='20px 1fr' alignItems='center'>
                    <EmailIcon w={3} h={3} />{' '}
                    <Text fontSize='sm'>{item.email}</Text>
                  </Grid>
                </Box>
                <Box mt='10' mb='2' align='center'>
                  <Button
                    colorScheme='blue'
                    onClick={() => history.push(`/profile/${item.id}`)}
                  >
                    See user profile
                  </Button>
                </Box>
              </Box>
            </GridItem>
          ))}
      </Grid>
      {renderPagination()}
    </>
  );
}
