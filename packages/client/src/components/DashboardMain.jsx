import React from 'react';
import { gql, useQuery } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import {
  Grid,
  GridItem,
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  Box,
  Badge,
  Button,
} from '@chakra-ui/react';

export const TOTAL_CLIENTS_BY_CITY = gql`
  query {
    totalCustomersByCity {
      city
      customers_total
    }
  }
`;

export default function DashboardMain() {
  const { loading, error, data } = useQuery(TOTAL_CLIENTS_BY_CITY);

  const history = useHistory();
  if (loading) {
    return <GridItem>Loading</GridItem>;
  }
  if (error) {
    return (
      <Alert status='error'>
        <AlertIcon />
        <AlertTitle mr={2}>Error fetching cities</AlertTitle>
        <AlertDescription>{error}</AlertDescription>
      </Alert>
    );
  }

  return (
    <Grid templateColumns='repeat(auto-fit, minmax(200px, 1fr))' gap={6}>
      {data.totalCustomersByCity.map((item) => (
        <GridItem key={item.city}>
          <Box
            maxW='sm'
            borderWidth='1px'
            borderRadius='lg'
            overflow='hidden'
            padding='20px'
          >
            <Box
              mt='2'
              mb='10'
              fontWeight='semibold'
              as='h4'
              lineHeight='tight'
              isTruncated
            >
              {item.city}
            </Box>
            <Badge ml='1' fontSize='1em' colorScheme='green'>
              {item.customers_total}
            </Badge>{' '}
            total customers
            <Box mt='10' mb='2' align='center'>
              <Button
                colorScheme='blue'
                onClick={() => history.push(`/cities/${item.city}`)}
              >
                See more
              </Button>
            </Box>
          </Box>
        </GridItem>
      ))}
    </Grid>
  );
}
