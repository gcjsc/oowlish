import React from 'react';
import { useParams } from 'react-router-dom';
import { gql, useQuery } from '@apollo/client';
import { Grid, GridItem, Box, Button, Text, Heading } from '@chakra-ui/react';
import {
  EmailIcon,
  ChevronDownIcon,
  SunIcon,
  MoonIcon,
} from '@chakra-ui/icons';
import { useHistory } from 'react-router-dom';
import MapContainer from './MapContainer';

export const CUSTOMERS_BY_ID = gql`
  query CustomersByID($id: Int!) {
    customer(id: $id) {
      id
      first_name
      last_name
      email
      gender
      company
      title
      lat
      lng
      city
    }
  }
`;

export default function Profile() {
  const { id } = useParams();
  const history = useHistory();

  const { loading, error, data } = useQuery(CUSTOMERS_BY_ID, {
    variables: { id: parseInt(id) },
    fetchPolicy: 'network-only',
  });

  if (loading) {
    return <div>Loading</div>;
  }

  if (!id || !data || error) {
    return <div>Customer not found</div>;
  }
  return (
    <>
      <Heading as='h2' size='2xl' mb='10'>
        {data.customer.first_name} {data.customer.last_name}
      </Heading>
      {data && (
        <Grid
          templateColumns='300px repeat(auto-fit, minmax(200px, 1fr))'
          gap='2'
          maxW='100%'
        >
          <GridItem key={data.customer.id}>
            <Box
              maxW='sm'
              borderWidth='1px'
              borderRadius='lg'
              overflow='hidden'
              padding='20px'
            >
              <Box
                mt='2'
                mb='10'
                fontWeight='semibold'
                as='h4'
                lineHeight='tight'
                isTruncated
              >
                {data.customer.gender === 'Male' ? (
                  <MoonIcon mr='2' />
                ) : (
                  <SunIcon mr='2' />
                )}{' '}
                {data.customer.first_name} {data.customer.last_name}
              </Box>
              <Box mt='10' mb='2' align='left'>
                <Text fontSize='sm'>
                  <strong>{data.customer.city}</strong>
                </Text>
              </Box>
              <Box mt='10' mb='2' align='left'>
                <Grid templateColumns='20px 1fr' alignItems='center'>
                  <ChevronDownIcon w={3} h={3} />{' '}
                  <Text fontSize='sm'>
                    {data.customer.title} at{' '}
                    <strong>{data.customer.company}</strong>
                  </Text>
                </Grid>
              </Box>
              <Box mb='2' align='left'>
                <Grid templateColumns='20px 1fr' alignItems='center'>
                  <EmailIcon w={3} h={3} />{' '}
                  <Text fontSize='sm'>{data.customer.email}</Text>
                </Grid>
              </Box>
              <Box mt='10' mb='2' align='center'>
                <Button colorScheme='gray' onClick={() => history.goBack()}>
                  Back
                </Button>
              </Box>
            </Box>
          </GridItem>
          <GridItem className='map' textAlign='center'>
            <MapContainer lat={data.customer.lat} lng={data.customer.lng} />
          </GridItem>
        </Grid>
      )}
    </>
  );
}
