import React from 'react';
import { Container, Button } from '@chakra-ui/react';
import { useAuth0 } from '@auth0/auth0-react';
import { useHistory } from 'react-router-dom';

export default function DashboardMenu() {
  const { isAuthenticated, logout } = useAuth0();
  const history = useHistory();

  return (
    <Container align='right' maxWidth='120ch' mb='20' h='80px' padding='30px'>
      {isAuthenticated && (
        <div>
          <Button
            role='button'
            colorScheme='green'
            size='sm'
            variant='outline'
            onClick={() => history.push(`/`)}
            mr='4'
          >
            Home
          </Button>
          <Button
            role='button'
            colorScheme='red'
            size='sm'
            variant='outline'
            onClick={() => logout()}
          >
            Logout
          </Button>
        </div>
      )}
    </Container>
  );
}
