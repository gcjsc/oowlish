import { render, screen, act } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import Profile, { CUSTOMERS_BY_ID } from '../components/Profile';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: () => ({ id: 1 }),
}));

const mocks = [
  {
    request: {
      query: CUSTOMERS_BY_ID,
      variables: {
        id: 1,
      },
    },
    result: {
      data: {
        customer: {
          id: 1,
          first_name: 'Laura',
          last_name: 'Richards',
          email: 'lrichards0@reverbnation.com',
          gender: 'Female',
          company: 'Meezzy',
          city: 'Warner, NH',
          title: 'Biostatistician III',
        },
      },
    },
  },
];

test('It renders without errors', () => {
  render(
    <MockedProvider mocks={mocks}>
      <Profile />
    </MockedProvider>
  );
  expect(screen.getByText('Loading'));
});

test('It renders the customer', async () => {
  render(
    <MockedProvider mocks={mocks}>
      <Profile />
    </MockedProvider>
  );
  await act(() => new Promise((resolve) => setTimeout(resolve, 0)));

  expect(screen.getAllByText('Laura Richards'));
  expect(screen.getByText('lrichards0@reverbnation.com'));
  expect(screen.getByText('Meezzy'));
  expect(screen.getByText('Biostatistician III at'));
  expect(screen.getByText('Back'));
});
