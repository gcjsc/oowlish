import { render, screen } from '@testing-library/react';
import { useAuth0 } from '@auth0/auth0-react';
import Menu from '../components/Menu';

jest.mock('@auth0/auth0-react');

test('A logged in user can see the menu', () => {
  useAuth0.mockReturnValue({ isAuthenticated: true });
  render(<Menu />);
  expect(screen.getByText(/Logout/i));
  expect(screen.getByText(/Home/i));
});

test('A logged out user cannot see the menu', () => {
  useAuth0.mockReturnValue({ isAuthenticated: false });
  render(<Menu />);
  expect(screen.queryByText(/Logout/i)).toBeNull();
  expect(screen.queryByText(/Home/i)).toBeNull();
});
