import { render, screen } from '@testing-library/react';
import Login from '../components/Login';

test('renders login form', () => {
  render(<Login />);
  const linkElement = screen.getByText(
    /You must be signed in in order to access the dashboard/i
  );
  expect(linkElement).toBeInTheDocument();
  expect(screen.getByRole('button')).toHaveTextContent(/Sign in/);
});
