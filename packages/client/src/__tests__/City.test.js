import { render, screen, act } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import City, { CUSTOMERS_BY_CITY } from '../components/City';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: () => ({ city: 'NY' }),
}));

const mocks = [
  {
    request: {
      query: CUSTOMERS_BY_CITY,
      variables: {
        city: 'NY',
      },
    },
    result: {
      data: {
        customersByCity: {
          cursor: 0,
          hasMore: true,
          hasLess: false,
          customers: [
            {
              id: 1,
              first_name: 'Laura',
              last_name: 'Richards',
              email: 'lrichards0@reverbnation.com',
              gender: 'Female',
              company: 'Meezzy',
              city: 'Warner, NH',
              title: 'Biostatistician III',
            },
            {
              id: 2,
              first_name: 'Margaret',
              last_name: 'Mendoza',
              email: 'mmendoza1@sina.com.cn',
              gender: 'Female',
              company: 'Skipfire',
              city: 'East Natchitoches, PA',
              title: 'VP Marketing',
            },
            {
              id: 3,
              first_name: 'Craig',
              last_name: 'Mccoy',
              email: 'cmccoy2@bluehost.com',
              gender: 'Male',
              company: 'Quatz',
              city: 'Lyon, WV',
              title: 'Senior Sales Associate',
            },
          ],
        },
      },
    },
  },
];

test('It renders without errors', () => {
  render(
    <MockedProvider mocks={mocks}>
      <City />
    </MockedProvider>
  );
  expect(screen.getByText('Loading'));
});

test('It renders the customers', async () => {
  render(
    <MockedProvider mocks={mocks}>
      <City />
    </MockedProvider>
  );
  await act(() => new Promise((resolve) => setTimeout(resolve, 0)));

  expect(screen.getByText('NY'));
  expect(screen.getByText('Laura Richards'));
  expect(screen.getByText('lrichards0@reverbnation.com'));
  expect(screen.getByText('Meezzy'));
  expect(screen.getByText('Biostatistician III at'));

  expect(screen.getByText('Margaret Mendoza'));
  expect(screen.getByText('mmendoza1@sina.com.cn'));
  expect(screen.getByText('Skipfire'));
  expect(screen.getByText('VP Marketing at'));
  expect(screen.getAllByText('See user profile'));
});

test('It shows the navigations buttons properly', async () => {
  render(
    <MockedProvider mocks={mocks}>
      <City />
    </MockedProvider>
  );
  await act(() => new Promise((resolve) => setTimeout(resolve, 0)));

  expect(screen.getByText('More')).toBeEnabled();
  expect(screen.getByText('Back')).toBeDisabled();
});
