import { render, screen, act } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import DashboardMain, {
  TOTAL_CLIENTS_BY_CITY,
} from '../components/DashboardMain';

const mocks = [
  {
    request: {
      query: TOTAL_CLIENTS_BY_CITY,
    },
    result: {
      data: {
        totalCustomersByCity: [
          {
            city: 'NY',
            customers_total: 10000,
          },
          {
            city: 'Boston',
            customers_total: 4000,
          },
        ],
      },
    },
  },
];

test('It renders without errors', () => {
  render(
    <MockedProvider mocks={mocks}>
      <DashboardMain />
    </MockedProvider>
  );
  expect(screen.getAllByText('Loading'));
});

test('It renders renders the cities', async () => {
  render(
    <MockedProvider mocks={mocks}>
      <DashboardMain />
    </MockedProvider>
  );
  await act(() => new Promise((resolve) => setTimeout(resolve, 0)));
  expect(screen.getAllByText('NY'));
  expect(screen.getAllByText(10000));
  expect(screen.getAllByText('Boston'));
  expect(screen.getAllByText(4000));
  expect(screen.getAllByText('See more'));
});
