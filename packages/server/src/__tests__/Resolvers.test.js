const ApolloClient = require('apollo-client').ApolloClient;
const gql = require('graphql-tag');
const createHttpLink = require('apollo-link-http').createHttpLink;
const InMemoryCache = require('apollo-cache-inmemory').InMemoryCache;
const fetch = require('node-fetch');

const startServer = require('../server');
const config = require('../config');

const httpLink = createHttpLink({
  uri: config.testEndpoint,
  fetch: fetch,
});

const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
});

let server;

beforeAll(async () => {
  server = await startServer(config.testPort);
});

const CUSTOMERS_BY_ID = gql`
  query CustomersByID($id: Int!) {
    customer(id: $id) {
      id
      first_name
      last_name
      lat
      lng
      city
    }
  }
`;

const CUSTOMERS_BY_CITY = gql`
  query CustomersByCity($city: String!, $cursor: Int) {
    customersByCity(city: $city, cursor: $cursor) {
      cursor
      hasMore
      hasLess
      customers {
        id
        first_name
        last_name
        email
        gender
        company
        title
      }
    }
  }
`;

const TOTAL_CLIENTS_BY_CITY = gql`
  query {
    totalCustomersByCity {
      city
      customers_total
    }
  }
`;

test('User can be fetched by id', async () => {
  const result = await client.query({
    query: CUSTOMERS_BY_ID,
    variables: { id: 1 },
  });

  const user = result.data.customer;

  expect(user.first_name).toBe('Laura');
  expect(user.id).toBe(1);
});

test('Lat and lng resolvers should be working', async () => {
  const result = await client.query({
    query: CUSTOMERS_BY_ID,
    variables: { id: 1 },
  });
  const user = result.data.customer;

  expect(user.lat).not.toBeNull();
  expect(user.lng).not.toBeNull();
});

test('Total customers by city can be fetched by city name', async () => {
  const result = await client.query({
    query: CUSTOMERS_BY_CITY,
    variables: { city: 'Warner, NH' },
  });
  const customers = result.data.customersByCity;

  expect(customers.customers.length).toBeGreaterThan(0);
  expect(customers.hasLess).toBeFalsy();
  expect(customers.hasMore).toBeTruthy();
});

test('Should get all cities with total users per each', async () => {
  const result = await client.query({
    query: TOTAL_CLIENTS_BY_CITY,
  });
  const cities = result.data.totalCustomersByCity;
  expect(cities.length).toBeGreaterThan(0);
});

afterAll(async () => {
  await server.close();
});
