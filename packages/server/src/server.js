require('dotenv').config();
const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const typeDefs = require('./graphql/schema');
const resolvers = require('./resolvers/userResolver');
const cors = require('cors');
const config = require('./config');

const start = (port = 3333) => {
  const app = express();
  app.use(
    cors({
      origin: config.client,
    })
  );

  const graphServer = new ApolloServer({
    typeDefs,
    resolvers,
  });

  graphServer.applyMiddleware({ app });

  return new Promise((resolve) => {
    const server = app.listen(port, () => {
      const originalClose = server.close.bind(server);
      server.close = () => {
        return new Promise((resolveClose) => {
          originalClose(resolveClose);
        });
      };
      resolve(server);
    });
  });
};

start();

module.exports = start;
