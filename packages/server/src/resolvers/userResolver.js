const fs = require('fs');
const userdata = JSON.parse(
  fs.readFileSync(__dirname + '/../data/customers.json')
);
const UserModel = require('../models/User');

const userResolver = {
  Query: {
    customer: (_, { id }) => {
      const user = userdata.find((user) => user.id === id);
      return UserModel(user);
    },
    totalCustomersByCity: () => {
      const cities = [...new Set(userdata.map((user) => user.city))];
      const teste = cities.map((city) => {
        const users = userdata.filter((user) => user.city === city);

        return {
          city,
          customers_total: users.length,
        };
      });
      return teste;
    },
    customersByCity: (_, { city, cursor }) => {
      const perPage = 5;
      let pointer;
      const newCursor = cursor ? cursor : 0;
      pointer = newCursor * perPage;

      const users = userdata.filter((user) => user.city === city);
      const hasMore = users.length > pointer + perPage;
      const hasLess = pointer - perPage >= 0;

      const slicedUsers = users.slice(pointer, pointer + perPage);

      return {
        hasMore,
        hasLess,
        cursor: newCursor,
        customers: slicedUsers.map((user) => UserModel(user)),
      };
    },
  },
};

module.exports = userResolver;
