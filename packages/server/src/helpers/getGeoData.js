const cachios = require('cachios');
const config = require('../config');

const getGeoData = async (address) => {
  const api = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${config.google_geo_api}`;
  try {
    const response = await cachios.get(api);
    return response.data.results[0].geometry.location;
  } catch (error) {
    console.log(
      '🚀 ~ file: getGeoData.js ~ line 11 ~ getGeoData ~ error',
      error
    );
  }
};

module.exports = getGeoData;
