const getGeoData = require('../helpers/getGeoData');
const geoData = require('../helpers/getGeoData');

const UserModel = async (data) => {
  const geo = await getGeoData(data.city);
  return {
    id: data.id,
    first_name: data.first_name,
    last_name: data.last_name,
    email: data.email,
    gender: data.gender,
    company: data.company,
    city: data.city,
    title: data.title,
    lat: geo.lat,
    lng: geo.lng,
  };
};

module.exports = UserModel;
