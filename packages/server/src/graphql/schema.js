const { gql } = require('apollo-server-express');

const schema = gql`
  type Query {
    customer(id: Int!): User
    totalCustomersByCity: [TotalCustomersByCity]
    customersByCity(city: String!, cursor: Int): CustomersByCity
  }

  type TotalCustomersByCity {
    city: String!
    customers_total: Int
  }

  type CustomersByCity {
    cursor: String!
    hasMore: Boolean!
    hasLess: Boolean!
    customers: [User]
  }

  type User {
    id: Int!
    first_name: String!
    last_name: String
    email: String!
    gender: String
    company: String
    city: String
    title: String
    lat: String
    lng: String
  }
`;
module.exports = schema;
