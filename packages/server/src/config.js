const config = {
  dev: process.env.NODE_ENV !== 'production',
  google_geo_api: process.env.GOOGLE_GEO_API_KEY,
  client: process.env.CLIENT_URL,
  testEndpoint: process.env.TEST_ENDPOINT,
  testPort: process.env.TEST_PORT,
};

module.exports = config;
